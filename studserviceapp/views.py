import os
import re
import datetime
import json
from distutils.util import strtobool
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.core import serializers
from django.core.files.storage import default_storage
from django.utils import timezone
from django.contrib import messages
from studserviceapp.models import Nalog, Student, Nastavnik, Semestar, \
    Termin, RasporedNastave, Predmet, IzbornaGrupa, IzborGrupe, Administracija, \
    RasporedPolaganja, TerminPolaganja, Obavestenje, Grupa
from studserviceapp.forms import FileUpload, FileUploadRequired, UnosRedKolokvijuma, \
    ImageUpload, UnosObavestenja
from studserviceapp.send_gmails import create_and_send_message
from studserviceapp.utils import import_raspored_kolokvijuma_from_csv, get_or_none, podaciOTerminima

def index(request):
    return HttpResponse("Dobrodošli na studentski servis")

def not_found(request):
    return render(request, 'studserviceapp/not_found.html')

def login(request):
    if request.method == 'POST':
        podaci = request.POST
        try:
            nalog = Nalog.objects.get(username=podaci['username'])
            return redirect('home', username=podaci['username'])
        except Nalog.DoesNotExist:
            messages.error(request, 'Nalog ne postoji')
    return render(request, 'studserviceapp/login.html')

def home(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        semestar = Semestar.objects.all().latest("id")
        raspored_nastave = RasporedNastave.objects.filter(semestar=semestar).latest("datum_unosa")
        obavestenja = Obavestenje.objects.all().order_by('-id')[:5]
        if nalog.uloga == 'student':
            student = Student.objects.get(nalog=nalog)
            grupa = student.grupa.get(semestar=semestar)
            termini = Termin.objects.filter(raspored=raspored_nastave, grupe=grupa)
            grupeZaPredmete = {}
            vremenaZaPredmete = {}
            grupeZaPredmete, vremenaZaPredmete = podaciOTerminima(termini)
            return render(request, 'studserviceapp/home.html', {'username': username, 'nalog': nalog, 'korisnik': student,
                'termini': termini, 'spisakGrupa': json.dumps(grupeZaPredmete), 'vremenaZaPredmete': json.dumps(vremenaZaPredmete),
                'obavestenja': obavestenja})
        elif nalog.uloga == "nastavnik":
            podaci = Nastavnik.objects.get(nalog=nalog)
            termini = Termin.objects.filter(raspored=raspored_nastave, nastavnik=podaci)
            grupeZaPredmete = {}
            vremenaZaPredmete = {}
            grupeZaPredmete, vremenaZaPredmete = podaciOTerminima(termini)
            return render(request, 'studserviceapp/home.html', {'username': username, 'nalog': nalog, 'korisnik': podaci,
                'termini': termini, 'spisakGrupa': json.dumps(grupeZaPredmete), 'vremenaZaPredmete': json.dumps(vremenaZaPredmete),
                'obavestenja': obavestenja})
        else:
            podaci = Administracija.objects.get(nalog=nalog)
            termini = Termin.objects.filter(raspored=raspored_nastave)
            grupeZaPredmete = {}
            vremenaZaPredmete = {}
            grupeZaPredmete, vremenaZaPredmete = podaciOTerminima(termini)
            return render(request, 'studserviceapp/home.html', {'username': username, 'nalog': nalog, 'korisnik': podaci,
                'termini': termini, 'spisakGrupa': json.dumps(grupeZaPredmete), 'vremenaZaPredmete': json.dumps(vremenaZaPredmete),
                'obavestenja': obavestenja})
    except Nalog.DoesNotExist:
        return redirect('login')

def raspored_nastave(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        semestar = Semestar.objects.all().latest("id")
        raspored_nastave = RasporedNastave.objects.filter(semestar=semestar).latest("datum_unosa")
        grupe = Grupa.objects.all()
        nastavnici = Nastavnik.objects.all()
        if request.method == 'POST':
            podaci = request.POST
            filter = podaci['filter_by']
            vrednost = podaci['izbor']
            if filter == 'grupa':
                g = Grupa.objects.get(oznaka_grupe=vrednost)
                termini = Termin.objects.filter(raspored=raspored_nastave, grupe=g)
            elif filter == 'nastavnik':
                n = Nastavnik.objects.get(id=vrednost)
                termini = Termin.objects.filter(raspored=raspored_nastave, nastavnik=n)
            else:
                termini = Termin.objects.filter(raspored=raspored_nastave, oznaka_ucionice=vrednost)
        else:
            termini = Termin.objects.filter(raspored=raspored_nastave)
        gr = list()
        for g in grupe:
            gr.append(g.oznaka_grupe)
        nas = list()
        for n in nastavnici:
            nas.append({'id': n.id, 'ime': n.ime + " " + n.prezime})
        grupeZaPredmete = {}
        vremenaZaPredmete = {}
        grupeZaPredmete, vremenaZaPredmete = podaciOTerminima(termini)
        sviTermini = Termin.objects.filter(raspored=raspored_nastave)
        ucionice = []
        for t in sviTermini:
            ucionice.append(t.oznaka_ucionice)
        ucionice = set(ucionice)
        return render(request, 'studserviceapp/raspored_nastave.html', { 'username' : username, 'nalog': nalog, 'termini': termini,
            'spisakGrupa': json.dumps(grupeZaPredmete), 'vremenaZaPredmete': json.dumps(vremenaZaPredmete), 'grupe': gr,
            'nastavnici': nastavnici, 'ucionice': list(ucionice), 'nastavniciFilter': json.dumps(nas)})
    except Nalog.DoesNotExist:
        return redirect('login')

def unos_izborne_grupe(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'administrator':
            pr = Predmet.objects.all()
            semestar = Semestar.objects.all()
            predmeti = list()
            for p in pr:
                predmeti.append({'naziv': p.naziv, 'semestar': p.semestar_po_programu})
            return render(request, 'studserviceapp/unos_izborne_grupe.html', { 'username': username, 'nalog': nalog,
                'predmeti' : json.dumps(predmeti), 'semestar': semestar })
        else:
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', {'username': username, 'nalog': nalog})
    except Nalog.DoesNotExist:
        return redirect('login')

def sacuvaj_izbornu_grupu(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        podaci = request.POST
        podaci_za_semestar=podaci['semestar'].split(' ')
        vrsta=podaci_za_semestar[0]
        godine=podaci_za_semestar[1].split('/')
        semestar=Semestar.objects.get(vrsta=vrsta, skolska_godina_pocetak=godine[0], skolska_godina_kraj=godine[1])

        oznakeGrupi = list(map(str.strip, podaci['oznaka_grupe'].split(',')))
        for g in oznakeGrupi:
            if IzbornaGrupa.objects.filter(oznaka_grupe=g, za_semestar=semestar).exists():
                tekst = 'Izborna grupa sa oznakom ' + g + ' u ovom semestru već postoji.'
                return render(request, 'studserviceapp/neuspesno.html', { 'username': username, 'nalog': nalog,
                    'naslov': tekst })

            izbornaGrupa = IzbornaGrupa.objects.create(oznaka_grupe=g, oznaka_semestra=podaci['oznaka_semestra'],
                kapacitet=podaci['kapacitet'], smer=podaci['smer'], aktivna=strtobool(podaci['aktivna']), za_semestar=semestar)

            predmeti = podaci.getlist('predmeti')
            for naziv in predmeti:
                predmet = Predmet.objects.get(naziv=naziv)
                izbornaGrupa.predmeti.add(predmet)
        return redirect('pregled_svih_izbornih_grupa', username=username)
    except Nalog.DoesNotExist:
        return redirect('login')


def izbor_grupe(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == "student":
            podaci = Student.objects.get(nalog=nalog)
            semestar = Semestar.objects.latest("id")
            if IzborGrupe.objects.filter(student_id=podaci.id, izabrana_grupa__za_semestar=semestar).exists():
                return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog,
                    'tekst': 'Grupa je vec izabrana!' })
            izborne_grupe = list(IzbornaGrupa.objects.filter(smer=podaci.smer, aktivna=True))
            predmeti = Predmet.objects.all()
            for index, grupa in enumerate(izborne_grupe):
                broj_koji_su_izabrali = IzborGrupe.objects.filter(izabrana_grupa=grupa.id).count()
                if broj_koji_su_izabrali >= grupa.kapacitet:
                    del izborne_grupe[index]
            grupe = list()
            for ig in izborne_grupe:
                grupe.append({'oznaka_grupe': ig.oznaka_grupe, 'oznaka_semestra': ig.oznaka_semestra })
            return render(request, 'studserviceapp/izbor_grupe.html', { 'username': username, 'nalog': nalog,
                'podaci': podaci, 'semestar': semestar, 'grupe': json.dumps(grupe), 'predmeti': predmeti })
        else:
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog,
                'tekst': 'Izabrani nalog nije student.' })
    except Nalog.DoesNotExist:
        return redirect('login')

def snimi_izbor(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        podaci = request.POST
        student = Student.objects.get(ime=podaci['ime'], prezime=podaci['prezime'], broj_indeksa=podaci['indeks'],
            godina_upisa=podaci['godina_upisa'], smer=podaci['smer'])
        izabrana_grupa = IzbornaGrupa.objects.get(oznaka_grupe=podaci['grupa'])
        grupa = IzborGrupe.objects.create(ostvarenoESPB=podaci['ukupno_ESPB'], upisujeESPB=podaci['upisujem_ESPB'],
            broj_polozenih_ispita=podaci['broj_polozenih_ispita'], upisuje_semestar=podaci['semestar'],
            prvi_put_upisuje_semestar=strtobool(podaci['prvi_put_upisujem']), nacin_placanja=podaci['nacin_placanja'],
            student=student, izabrana_grupa=izabrana_grupa, upisan=False)

        for predmet in podaci.getlist('predmeti'):
            p = Predmet.objects.get(naziv=predmet)
            grupa.nepolozeni_predmeti.add(p)
        return render(request, 'studserviceapp/uspesno.html', { 'username': username, 'nalog': nalog, 'tekst': 'Izbor grupe sačuvan!' })
    except Nalog.DoesNotExist:
        return redirect('login')

def izmeni_grupu(request, username, oznaka_grupe):
    try:
        nalog = Nalog.objects.get(username=username)
        grupa = IzbornaGrupa.objects.get(oznaka_grupe=oznaka_grupe)
        semestar = Semestar.objects.all()
        predmeti = grupa.predmeti.all() #Get object from many to many !!
        svi_predmeti = Predmet.objects.all()
        return render(request, 'studserviceapp/izmeni_grupu.html', { 'username': username, 'nalog': nalog, 'grupa': grupa,
            'semestar': semestar, 'predmeti': predmeti, 'svi_predmeti': svi_predmeti})
    except IzbornaGrupa.DoesNotExist:
        return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog,
            'tekst': 'Ne postoji ova izborna grupa.' })
    except Nalog.DoesNotExist:
        return redirect('login')

def sacuvaj_izmene_grupe(request, username, oznaka_grupe):
    try:
        nalog = Nalog.objects.get(username=username)
        podaci=request.POST
        grupa=IzbornaGrupa.objects.get(oznaka_grupe=oznaka_grupe)
        grupa.oznaka_semestra=podaci['oznaka_semestra']

        podaci_za_semestar=podaci['semestar'].split(' ')
        vrsta=podaci_za_semestar[0]
        godine=podaci_za_semestar[1].split('/')
        semestar=Semestar.objects.get(vrsta=vrsta, skolska_godina_pocetak=godine[0], skolska_godina_kraj=godine[1])
        grupa.za_semestar=semestar

        grupa.kapacitet=podaci['kapacitet']
        grupa.smer=podaci['smer']
        grupa.aktivna=strtobool(podaci['aktivna'])
        #prvo da obrisem sve
        grupa.predmeti.clear()
        #pa ubacim nove
        predmeti=podaci.getlist('predmeti')
        for naziv in predmeti:
            predmet=Predmet.objects.get(naziv=naziv)
            grupa.predmeti.add(predmet)
        grupa.save()
        return redirect('pregled_svih_izbornih_grupa', username=username)
    except Nalog.DoesNotExist:
        return redirect('login')

def pregled_izabranih_grupa(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'student' or nalog.uloga == 'nastavnik':
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog })
        else:
            semestar = Semestar.objects.latest("id")
            izabrane_grupe = IzborGrupe.objects.filter(izabrana_grupa__za_semestar=semestar).order_by('izabrana_grupa').distinct('izabrana_grupa')
            return render(request, 'studserviceapp/pregled_izabranih_grupa.html', { 'username': username, 'nalog': nalog,
            'grupe': izabrane_grupe, 'semestar': semestar })
    except Nalog.DoesNotExist:
        return redirect('login')

def pregled_grupe(request, username, oznaka_grupe):
    try:
        nalog = Nalog.objects.get(username=username)
        grupa = IzbornaGrupa.objects.get(oznaka_grupe=oznaka_grupe)
        izabrana_grupa = IzborGrupe.objects.filter(izabrana_grupa=grupa)
        studenti = []
        for izbor in izabrana_grupa:
            studenti.append(izbor.student)
        return render(request, 'studserviceapp/pregled_grupe.html', { 'username': username, 'nalog': nalog, 'grupa': oznaka_grupe,
            'studenti': studenti })
    except IzbornaGrupa.DoesNotExist:
        return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog,
            'tekst': 'Nema studenata koji su izabrali ovu grupu.' })
    except Nalog.DoesNotExist:
        return redirect('login')

def pregled_svih_izbornih_grupa(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'administrator' or nalog.uloga == 'sekretar':
            if request.method == 'POST':
                podaci = request.POST
                if podaci['smer'] and not podaci['oznaka_semestra']:
                    grupe = IzbornaGrupa.objects.filter(smer=podaci['smer']).order_by('oznaka_grupe')
                elif podaci['oznaka_semestra'] and not podaci['smer']:
                    grupe = IzbornaGrupa.objects.filter(oznaka_semestra=podaci['oznaka_semestra']).order_by('oznaka_grupe')
                elif podaci['oznaka_semestra'] and podaci['smer']:
                    grupe = IzbornaGrupa.objects.filter(smer=podaci['smer'], oznaka_semestra=podaci['oznaka_semestra']).order_by('oznaka_grupe')
                else:
                    grupe = IzbornaGrupa.objects.all().order_by('oznaka_grupe')
            else:
                grupe = IzbornaGrupa.objects.all().order_by('oznaka_grupe')

            predmetiPoGrupama = {}
            for g in grupe:
                predmeti = list()
                pr = g.predmeti.all()
                for p in pr:
                    predmeti.append(p.naziv)
                predmetiPoGrupama[g.oznaka_grupe] = predmeti
            return render(request, 'studserviceapp/pregled_svih_izbornih_grupa.html', { 'username': username, 'nalog': nalog,
                'grupe': grupe, 'predmetiPoGrupama': json.dumps(predmetiPoGrupama) })
        else:
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', {'username': username, 'nalog': nalog})
    except Nalog.DoesNotExist:
        return redirect('login')

def podaci(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'student':
            podaci = Student.objects.get(nalog=nalog)
            grupe = IzborGrupe.objects.filter(student=podaci)
            print(podaci.slika)
            return render(request, 'studserviceapp/student_podaci.html', { 'username': username, 'nalog': nalog, 'podaci': podaci, 'grupe': grupe })
        elif nalog.uloga == 'nastavnik':
            podaci = Nastavnik.objects.get(nalog=nalog)
            termini = Termin.objects.filter(nastavnik=podaci)
            predmetPodaci = []
            predmeti = []
            for t in termini.distinct('predmet'):
                predmeti.append(t.predmet)
            for p in predmeti:
                spisakGrupa = []
                pred = Predmet.objects.get(naziv=p)
                terminiZaPredmet = Termin.objects.filter(nastavnik=podaci, predmet=pred)
                for t in terminiZaPredmet:
                    grupe = t.grupe.all()
                    for g in grupe:
                        spisakGrupa.append(g.oznaka_grupe)
                predmetPodaci.append({'predmet': p, 'grupe': spisakGrupa})
            return render(request, 'studserviceapp/profesor_podaci.html', { 'username': username, 'nalog': nalog, 'podaci': podaci, 'predmetPodaci': predmetPodaci })
        else:
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog })
    except Nalog.DoesNotExist:
        return redirect('login')

def upload_slike(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if request.method == 'POST':
          form = ImageUpload(request.POST, request.FILES)
          if form.is_valid():
             student = Student.objects.get(nalog=nalog)
             student.slika = form.cleaned_data['image']
             student.save()
             return redirect('podaci', username=username)
        else:
          form = ImageUpload()
        return render(request, 'studserviceapp/upload_slike.html', { 'username': username, 'nalog': nalog, 'form': form })
    except Nalog.DoesNotExist:
        return redirect('login')

def slika_studenta(request, username, grupa, usernameStudenta):
    try:
        nalogKorisnika = Nalog.objects.get(username=username)
        nalog = Nalog.objects.get(username=usernameStudenta)
        podaci = Student.objects.get(nalog=nalog)
        return render(request, 'studserviceapp/slika_studenta.html', { 'username': username, 'nalog': nalogKorisnika,
            'podaci': podaci, 'grupa': grupa })
    except Nalog.DoesNotExist:
        return redirect('login')

def upload_rasporeda(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'student' or nalog.uloga == 'nastavnik':
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog })
        else:
            fileForm = FileUploadRequired()
            return render(request, 'studserviceapp/upload_rasporeda.html', { 'username': username, 'nalog': nalog, 'form': fileForm })
    except Nalog.DoesNotExist:
        return redirect('login')


def rezultati_unosa(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'student' or nalog.uloga == 'nastavnik':
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog })
        else:
            podaci = request.POST
            file = request.FILES.get('file')
            if file is not None:
                with default_storage.open(file.name, 'wb') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                rp, created = RasporedPolaganja.objects.get_or_create(kolokvijumska_nedelja=podaci['kolokvijumska_nedelja'])
                greske, ids = import_raspored_kolokvijuma_from_csv(default_storage.path(file.name), rp)
                request.session['ids'] = ids
                return render(request, 'studserviceapp/rezultati_unosa_rasporeda.html', { 'username': username, 'nalog': nalog,
                    'greske': greske, 'brojGreski': len(greske)})
            else:
                odluka = podaci['regulacija']
                if int(odluka) == 1:
                    ids = request.session['ids']
                    for id in ids:
                        termin = TerminPolaganja.objects.get(id=id)
                        termin.nastavnik.clear()
                        termin.delete()
                    del request.session['ids']
                    return redirect('upload_rasporeda', username=username)
                else:
                    del request.session['ids']
                    return redirect('dodavanje_u_raspored_kolokvijuma', username=username)
    except Nalog.DoesNotExist:
        return redirect('login')

def dodavanje_u_raspored_kolokvijuma(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'student' or nalog.uloga == 'nastavnik':
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', { 'username': username, 'nalog': nalog })
        else:
            if request.method == 'POST':
                form = UnosRedKolokvijuma(request.POST)
                if form.is_valid():
                    podaci = request.POST
                    predmet = Predmet.objects.get(naziv=podaci['predmet'])
                    nizProfesora = [p.strip(' ') for p in podaci['profesor'].split(',')]
                    profesori = []
                    for p in nizProfesora:
                        imePrezime = p.split()
                        ime = imePrezime.pop(0)
                        prezime = " ".join(imePrezime)
                        prof = Nastavnik.objects.get(ime=ime, prezime=prezime)
                        profesori.append(prof)
                        rp, created = RasporedPolaganja.objects.get_or_create(kolokvijumska_nedelja=podaci['kolokvijumska_nedelja'])
                        termin, c = TerminPolaganja.objects.get_or_create(
                            ucionice=podaci['ucionice'],
                            pocetak=podaci['pocetak'],
                            zavrsetak=podaci['kraj'],
                            datum=datetime.date(int(podaci['datum_year']), int(podaci['datum_month']), int(podaci['datum_day'])),
                            raspored_polaganja=rp,
                            predmet=predmet)
                        for p in profesori:
                            termin.nastavnik.add(p)
                    return redirect('termin_dodat', username=username)
            else:
                form = UnosRedKolokvijuma()
            return render(request, 'studserviceapp/dodavanje_u_raspored_kolokvijuma.html', {'username': username, 'nalog': nalog, 'form': form})
    except Nalog.DoesNotExist:
        return redirect('login')

def termin_dodat(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        return render(request, 'studserviceapp/termin_dodat.html', {'username': username, 'nalog': nalog})
    except Nalog.DoesNotExist:
        return redirect('login')

def slanje_mejla(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'student':
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', {'username': username, 'nalog': nalog})
        elif nalog.uloga == 'nastavnik':
            fileForm = FileUpload()
            podaci = Nastavnik.objects.get(nalog=nalog)
            semestar = Semestar.objects.latest("id")
            raspored_nastave = RasporedNastave.objects.filter(semestar=semestar).latest("datum_unosa")
            predmeti = set(map(lambda t: t.predmet, Termin.objects.filter(raspored=raspored_nastave, nastavnik=podaci)))
            grupe = list(map(lambda g: g.izabrana_grupa.oznaka_grupe,
                IzborGrupe.objects.filter(izabrana_grupa__za_semestar=semestar, izabrana_grupa__predmeti__in=predmeti).order_by('izabrana_grupa').distinct('izabrana_grupa')))

            return render(request, 'studserviceapp/slanje_mejla.html', { 'username': username, 'nalog': nalog,
                'form': fileForm, 'nalog': nalog, 'podaci': podaci, 'predmeti': predmeti, 'grupe': grupe })
        else:
            fileForm = FileUpload()
            podaci = Administracija.objects.get(nalog=nalog)
            smerovi = set(map(lambda s: s.smer, Student.objects.all()))
            predmeti = list(map(lambda p: p.naziv, Predmet.objects.all()))
            semestar = Semestar.objects.latest("id")
            izabrane_grupe = list(map(lambda g: g.izabrana_grupa.oznaka_grupe,
                IzborGrupe.objects.filter(izabrana_grupa__za_semestar=semestar).order_by('izabrana_grupa').distinct('izabrana_grupa')))

            return render(request, 'studserviceapp/slanje_mejla.html', { 'username': username, 'nalog': nalog,
                'form': fileForm, 'nalog': nalog, 'podaci': podaci, 'smerovi': smerovi, 'predmeti': predmeti, 'grupe': izabrane_grupe })
    except Nalog.DoesNotExist:
        return redirect('login')

def posalji_mejl(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        podaci = request.POST
        file = request.FILES.get('file')
        if file is not None:
            with default_storage.open(file.name, 'wb') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            fileName = [default_storage.path(file.name)]
        else:
            fileName = None

        primalaci = podaci['kome']
        if primalaci == 'svima':
            studentski_mejlovi = list(map(lambda s: s.nalog.username + '@raf.rs', Student.objects.all()))
        else:
            splited = primalaci.split('_')
            if splited[0] == 'smer':
                studentski_mejlovi = list(map(lambda s: s.nalog.username + '@raf.rs', Student.objects.filter(smer=splited[1])))
            elif splited[0] == 'predmet':
                semestar = Semestar.objects.latest("id")
                grupe = list(map(lambda g: g.oznaka_grupe, IzbornaGrupa.objects.filter(za_semestar=semestar, predmeti__naziv=splited[1])))
                studentski_mejlovi = list(map(lambda s: s.nalog.username + '@raf.rs', Student.objects.filter(grupa__oznaka_grupe__in=grupe, grupa__semestar=semestar)))
            elif splited[0] == 'grupa':
                semestar = Semestar.objects.latest("id")
                studentski_mejlovi = list(map(lambda s: s.nalog.username + '@raf.rs', Student.objects.filter(grupa__oznaka_grupe=splited[1], grupa__semestar=semestar)))

        for s in studentski_mejlovi:
            create_and_send_message(podaci['mejl'], s, podaci['naslov'], podaci['poruka'], fileName)
        return render(request, 'studserviceapp/uspesno.html', { 'username': username, 'nalog': nalog, 'tekst': 'Mejl poslat!' })
    except Nalog.DoesNotExist:
        return redirect('login')

def postavljanje_obavestenja(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'student' or nalog.uloga == 'nastavnik':
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', {'username': username, 'nalog': nalog})
        else:
            if request.method == 'POST':
                form = UnosObavestenja(request.POST, request.FILES)
                if form.is_valid():
                    datum_unosa = timezone.now()
                    obavestenje = Obavestenje.objects.create(postavio=nalog, datum_postavljanja=datum_unosa)
                    obavestenje.tekst = form.cleaned_data['obavestenje']
                    obavestenje.fajl = form.cleaned_data['file']
                    obavestenje.save()
                    return redirect('home', username=username)
            else:
                form = UnosObavestenja()
                return render(request, 'studserviceapp/postavljanje_obavestenja.html', {'form': form, 'username': username, 'nalog': nalog})
    except Nalog.DoesNotExist:
        return redirect('login')

def unos_semestra(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'administrator':
            if request.method == 'POST':
                podaci = request.POST
                semestar, created = Semestar.objects.get_or_create(vrsta=podaci['vrsta_semestra'],
                    skolska_godina_pocetak=podaci['pocetak_skolske_godine'], skolska_godina_kraj=podaci['kraj_skolske_godine'])
                return redirect('home', username=username)
            else:
                return render(request, 'studserviceapp/unos_semestra.html', {'username': username, 'nalog': nalog})
        else:
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', {'username': username, 'nalog': nalog})
    except Nalog.DoesNotExist:
        return redirect('login')

def spisak_studenata_po_grupama(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'administrator' or nalog.uloga == 'sekretar':
            semestar = Semestar.objects.all().latest("id")
            grupe = IzbornaGrupa.objects.filter(za_semestar=semestar).order_by('oznaka_grupe')

            sredjeneGrupe = list()
            for g in grupe:
                studenti = list()
                izabranaGrupa = IzborGrupe.objects.filter(izabrana_grupa=g)
                for izbor in izabranaGrupa:
                    studenti.append(izbor.student)
                    if izbor.student.slika:
                        print(izbor.student.slika.url)
                sredjeneGrupe.append({'grupa': g.oznaka_grupe, 'studenti': studenti})

            return render(request, 'studserviceapp/spisak_studenata_po_grupama.html', {'username': username, 'nalog': nalog,
                'grupe': sredjeneGrupe})
        else:
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', {'username': username, 'nalog': nalog})
    except Nalog.DoesNotExist:
        return redirect('login')

def pretraga_studenta(request, username):
    try:
        nalog = Nalog.objects.get(username=username)
        if nalog.uloga == 'student':
            return render(request, 'studserviceapp/nije_dozvoljen_pristup.html', {'username': username, 'nalog': nalog})
        else:
            student = []
            poslednjiPodaci = None
            if request.method == 'POST':
                podaci = request.POST
                semestar = Semestar.objects.all().latest("id")
                if podaci['ime_prezime'] and not podaci['indeks']:
                    imeParsirano = podaci['ime_prezime'].split(" ")
                    ime = imeParsirano[0]
                    del imeParsirano[0]
                    prezime = " ".join(imeParsirano)
                    student = Student.objects.filter(ime=ime, prezime=prezime)
                elif podaci['indeks'] and not podaci['ime_prezime']:
                    indeksParsirano =  podaci['indeks'].split(" ")
                    smer = indeksParsirano[0]
                    indeks = indeksParsirano[1].split("/")
                    brojIndeksa = indeks[0]
                    godinaUpisa = indeks[1]
                    student = Student.objects.filter(broj_indeksa=brojIndeksa, godina_upisa=godinaUpisa, smer=smer)
                elif podaci['indeks'] and podaci['ime_prezime']:
                    imeParsirano = podaci['ime_prezime'].split(" ")
                    ime = imeParsirano[0]
                    del imeParsirano[0]
                    prezime = " ".join(imeParsirano)
                    indeksParsirano =  podaci['indeks'].split(" ")
                    smer = indeksParsirano[0]
                    indeks = indeksParsirano[1].split("/")
                    brojIndeksa = indeks[0]
                    godinaUpisa = indeks[1]
                    student = Student.objects.filter(ime=ime, prezime=prezime, broj_indeksa=brojIndeksa, godina_upisa=godinaUpisa, smer=smer)

                if len(student):
                    for s in student:
                        podaci = IzborGrupe.objects.filter(student=s)
                        for p in podaci:
                            if p.izabrana_grupa.za_semestar == semestar:
                                poslednjiPodaci = p

            return render(request, 'studserviceapp/pretraga.html', {'username': username, 'nalog': nalog, 'student': student,
                'poslednjiPodaci': poslednjiPodaci})
    except Nalog.DoesNotExist:
        return redirect('login')
