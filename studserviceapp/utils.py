import csv
import datetime
import re
from django.utils import timezone
from django.core.files.storage import default_storage
from studserviceapp.models import Grupa, Nastavnik, Termin, RasporedNastave, \
    Predmet, Nalog, Semestar, RasporedPolaganja, TerminPolaganja

daniNedelje = ["Ponedeljak", "Utorak", "Sreda", "Cetvrtak", "Petak", "Subota", "Nedelja"]

class VrstaCasa:
    def __init__(self, tip, pozicija):
        self.tip = tip
        self.pozicija = pozicija

class Greska:
    def __init__(self, red, poruka):
        self.red = red
        self.poruka = poruka

def get_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None

def import_timetable_from_csv(file_path):
    with open(file_path, encoding='utf-8') as csvfile:
        raspored_csv = csv.reader(csvfile, delimiter=';')
        next(raspored_csv)
        info = next(raspored_csv)
        spisak_vrste_casova = []

        for index, obj in enumerate(info):
            if obj == 'Predavanja':
                spisak_vrste_casova.append(VrstaCasa("Predavanja", index))
            elif obj == 'Praktikum':
                spisak_vrste_casova.append(VrstaCasa("Praktikum", index))
            elif obj == 'Vezbe':
                spisak_vrste_casova.append(VrstaCasa("Vezbe", index))
            elif obj == 'Predavanja i vezbe':
                spisak_vrste_casova.append(VrstaCasa("Predavanja i vezbe", index))

        semestar, s_created = Semestar.objects.get_or_create(vrsta="neparni", skolska_godina_pocetak=2018, skolska_godina_kraj=2019)
        raspored_nastave, rn_created = RasporedNastave.objects.get_or_create(
            datum_unosa = timezone.now(),
            semestar = semestar
        )

        for red in raspored_csv:
            predmet, p_created = Predmet.objects.get_or_create(naziv=red[0])
            next(raspored_csv)
            predmetInfo = next(raspored_csv)
            while len(predmetInfo) != 0:
                for vrsta_casa in spisak_vrste_casova:
                    pozicija = vrsta_casa.pozicija
                    nastavnikInfo = predmetInfo[pozicija]
                    if(nastavnikInfo):
                        imePrezime = nastavnikInfo.split()
                        ime = imePrezime.pop(len(imePrezime) - 1)
                        prezime = " ".join(imePrezime)
                        nalog = (ime[0] + prezime).replace(" ", "").lower()
                        nalog_baza, nalog_created = Nalog.objects.get_or_create(username = nalog, uloga="nastavnik")
                        nastavnik, nastavnik_created = Nastavnik.objects.get_or_create(ime = ime, prezime = prezime, nalog = nalog_baza)

                        grupe = predmetInfo[pozicija + 2].split(", ")
                        dan = predmetInfo[pozicija + 4]
                        cas = predmetInfo[pozicija + 5].split("-")
                        sat = cas[0].split(":")
                        ucionica = predmetInfo[pozicija + 6]
                        termin, t_created = Termin.objects.get_or_create(
                            oznaka_ucionice = ucionica,
                            pocetak = datetime.time(int(sat[0]), int(sat[1])),
                            zavrsetak = datetime.time(int(cas[1])),
                            dan = dan,
                            tip_nastave = vrsta_casa.tip,
                            nastavnik = nastavnik,
                            predmet = predmet,
                            raspored = raspored_nastave
                        )
                        for grupa in grupe:
                            g, g_created = Grupa.objects.get_or_create(oznaka_grupe = grupa, semestar = semestar)
                            termin.grupe.add(g)
                predmetInfo=next(raspored_csv)

def import_raspored_kolokvijuma_from_csv(file_path, raspored_polaganja):
    with open(file_path, encoding='utf-8') as csvfile:
        raspored_csv = csv.reader(csvfile, delimiter=',')
        next(raspored_csv)
        brojReda = 2
        greske = []
        generisaniIds = []
        for red in raspored_csv:
            pred = red[0]
            prof = [p.strip(' ') for p in red[3].split(',')]
            ucionice = red[4]
            vreme = red[5]
            dan = red[6]
            datum = red[7]
            predmet = get_or_none(Predmet, naziv=pred)
            if predmet != None:
                profesori = []
                for pr in prof:
                    imePrezime = pr.split()
                    ime = imePrezime.pop(0)
                    prezime = " ".join(imePrezime)
                    nas = get_or_none(Nastavnik, ime=ime, prezime=prezime)
                    profesorGreska = 0
                    if nas != None:
                        profesori.append(nas)
                    else:
                        greske.append(Greska(brojReda, 'Nastavnik ne postoji: ' + pr))
                        profesorGreska += 1
                if profesorGreska == 0:
                    if re.fullmatch(r"(([1-9][0-9]?|R[gG][1-9]|Atelje), ?)*([1-9][0-9]?|R[gG][1-9]|Atelje)", ucionice) != None:
                        if re.fullmatch(r"(0[1-9]|1[0-9]|2[0-3])-(0[1-9]|1[0-9]|2[0-3])", vreme) != None:
                            parsiranoVreme = vreme.split('-')
                            if dan in daniNedelje:
                                if re.fullmatch(r"(0?[1-9]|[12][0-9]|3[01]).(0?[1-9]|1[12]).", datum) != None:
                                    parsiranDatum = datum.split('.')
                                    year = datetime.datetime.now().year
                                    termin, c = TerminPolaganja.objects.get_or_create(
                                        ucionice=ucionice,
                                        pocetak=datetime.time(int(parsiranoVreme[0])),
                                        zavrsetak=datetime.time(int(parsiranoVreme[1])),
                                        datum=datetime.date(year, int(parsiranDatum[1]), int(parsiranDatum[0])),
                                        raspored_polaganja=raspored_polaganja,
                                        predmet=predmet)
                                    for p in profesori:
                                        termin.nastavnik.add(p)
                                    generisaniIds.append(termin.id)
                                else:
                                    greske.append(Greska(brojReda, 'Datum je loseg formata. Treba biti formata "DD.MM."'))
                            else:
                                greske.append(Greska(brojReda, 'Dan u nedelji je loseg formata: ' + dan))
                        else:
                            greske.append(Greska(brojReda, 'Vreme je loseg formata. Treba biti formata "HH-HH"'))
                    else:
                        greske.append(Greska(brojReda, 'Nemoguce je parsirati ucionice. Uneti ih u formatu "ucionica,ucionica,..."'))
            else:
                greske.append(Greska(brojReda, 'Predmet ne postoji: ' + pred))
            brojReda += 1
    default_storage.delete(file_path)
    return greske, generisaniIds

def podaciOTerminima(termini):
    grupeZaPredmete = {}
    vremenaZaPredmete = {}
    for t in termini:
        grupe = t.grupe.all()
        imenaGrupa = list(map(lambda g: g.oznaka_grupe, grupe))
        grupeSredjeno = ", ".join(imenaGrupa)
        vreme = t.pocetak.strftime("%H:%M") + " - " + t.zavrsetak.strftime("%H:%M")
        grupeZaPredmete[t.id] = grupeSredjeno
        vremenaZaPredmete[t.id] = vreme
    return (grupeZaPredmete, vremenaZaPredmete)
