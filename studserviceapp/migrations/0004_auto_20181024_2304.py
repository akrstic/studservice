# Generated by Django 2.1.2 on 2018-10-24 23:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('studserviceapp', '0003_auto_20181024_2256'),
    ]

    operations = [
        migrations.AlterField(
            model_name='termin',
            name='tip_nastave',
            field=models.CharField(max_length=25),
        ),
    ]
