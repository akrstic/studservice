from django import forms
import datetime
import re
from studserviceapp.utils import get_or_none
from studserviceapp.models import Nastavnik, Predmet

class FileUpload(forms.Form):
    file = forms.FileField(label='Dodatak', required=False)

class FileUploadRequired(forms.Form):
    file = forms.FileField(label='Dodatak')

class ImageUpload(forms.Form):
    image = forms.ImageField(label="Slika")

class UnosRedKolokvijuma(forms.Form):
    kolokvijumska_nedelja = forms.CharField(label='Redni broj kolokvijumske nedelje u obliku "1."', max_length=10)
    predmet = forms.CharField(label='Naziv predmet', max_length=200)
    profesor = forms.CharField(label='Ime pa Prezime profesora razdvojeni zarezom', max_length=401)
    ucionice = forms.CharField(label='Ucionice oblika UC1,UC2,...', max_length=100)
    pocetak = forms.TimeField(label='Pocetak oblika HH:MM')
    kraj = forms.TimeField(label='Kraj oblika HH:MM')
    datum = forms.DateField(label="Datum", widget=forms.SelectDateWidget(years=[datetime.datetime.now().year + 1, datetime.datetime.now().year, datetime.datetime.now().year - 1], empty_label=("Godina", "Mesec", "Dan")))

    def clean_kolokvijumska_nedelja(self):
        data = self.cleaned_data['kolokvijumska_nedelja']
        validated = re.fullmatch(r"[1-4].", data)
        if validated == None:
            self.add_error('kolokvijumska_nedelja', "Format kolokvijumske nedelje nije validan")

    def clean_predmet(self):
        data = self.cleaned_data['predmet']
        predmet = get_or_none(Predmet, naziv=data)
        if predmet == None:
            self.add_error('predmet', "Naziv predmeta nije validan")

    def clean_profesor(self):
        data = self.cleaned_data['profesor']
        nizProfesora = [p.strip(' ') for p in data.split(',')]
        profesori = []
        profNotFound = False
        for p in nizProfesora:
            imePrezime = p.split()
            ime = imePrezime.pop(0)
            prezime = " ".join(imePrezime)
            prof = get_or_none(Nastavnik, ime=ime, prezime=prezime)
            if prof == None:
                profNotFound = True
            else:
                profesori.append(prof)
        if profNotFound:
            self.add_error('profesor', "Neki od profesora nije pronadjen ili nedostaje zarez")

    def clean_pocetak(self):
        data = self.cleaned_data['ucionice']
        if re.fullmatch(r"(([1-9][0-9]?|R[gG][1-9]|Atelje), ?)*([1-9][0-9]?|R[gG][1-9]|Atelje)", data) is None:
            self.add_error('ucionice', "Ucionice nisu dobrog formata ili validne")

class UnosObavestenja(forms.Form):
    obavestenje = forms.CharField(widget=forms.Textarea(attrs={'cols': 50, 'rows': 5, 'class': 'form-control'}))
    file = forms.FileField(label='Fajl', required=False)
