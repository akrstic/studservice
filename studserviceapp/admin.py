from django.contrib import admin
from .models import IzbornaGrupa, Termin, Grupa

# Register your models here.
admin.site.register(IzbornaGrupa)
admin.site.register(Termin)
admin.site.register(Grupa)
