from django.urls import path, re_path
from . import views

urlpatterns = [
    path('index', views.index, name='index'),
    path('login', views.login, name="login"),
    path('home/<str:username>', views.home, name='home'),
    path('raspored_nastave/<str:username>', views.raspored_nastave, name='raspored_nastave'),
    path('unos_izborne_grupe/<str:username>', views.unos_izborne_grupe, name='unos_izborne_grupe'),
    path('sacuvaj_izbornu_grupu/<str:username>', views.sacuvaj_izbornu_grupu, name='sacuvaj_izbornu_grupu'),
    path('izbor_grupe/<str:username>', views.izbor_grupe, name='izbor_grupe'),
    path('snimi_izbor/<str:username>', views.snimi_izbor, name='snimi_izbor'),
    path('izmeni_grupu/<str:username>/<str:oznaka_grupe>', views.izmeni_grupu, name='izmeni_grupu'),
    path('sacuvaj_izmene_grupe/<str:username>/<str:oznaka_grupe>', views.sacuvaj_izmene_grupe, name='sacuvaj_izmene_grupe'),
    path('pregled_izabranih_grupa/<str:username>', views.pregled_izabranih_grupa, name='pregled_izabranih_grupa'),
    path('pregled_grupe/<str:username>/<str:oznaka_grupe>', views.pregled_grupe, name='pregled_grupe'),
    path('slanje_mejla/<str:username>', views.slanje_mejla, name='slanje_mejla'),
    path('posalji_mejl/<str:username>', views.posalji_mejl, name='posalji_mejl'),
    path('upload_rasporeda/<str:username>', views.upload_rasporeda, name='upload_rasporeda'),
    path('rezultati_unosa/<str:username>', views.rezultati_unosa, name='rezultati_unosa'),
    path('dodavanje_u_raspored_kolokvijuma/<str:username>', views.dodavanje_u_raspored_kolokvijuma, name='dodavanje_u_raspored_kolokvijuma'),
    path('termin_dodat/<str:username>', views.termin_dodat, name='termin_dodat'),
    path('podaci/<str:username>', views.podaci, name='podaci'),
    path('upload_slike/<str:username>', views.upload_slike, name='upload_slike'),
    path('slika_studenta/<str:username>/<str:grupa>/<str:usernameStudenta>', views.slika_studenta, name='slika_studenta'),
    path('postavljanje_obavestenja/<str:username>', views.postavljanje_obavestenja, name='postavljanje_obavestenja'),
    path('pregled_svih_izbornih_grupa/<str:username>', views.pregled_svih_izbornih_grupa, name='pregled_svih_izbornih_grupa'),
    path('unos_semestra/<str:username>', views.unos_semestra, name='unos_semestra'),
    path('spisak_studenata_po_grupama/<str:username>', views.spisak_studenata_po_grupama, name='spisak_studenata_po_grupama'),
    path('pretraga_studenta/<str:username>', views.pretraga_studenta, name='pretraga_studenta'),
    re_path(r'^.*/$', views.not_found, name="not_found"),
]
