import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'studservice.settings')

import django
django.setup()

from studserviceapp.models import Student, Grupa, Nalog, Semestar

class StudentInfo:
    def __init__(self, ime, prezime, broj_indeksa, godina_upisa, smer, grupa):
        self.ime = ime
        self.prezime = prezime
        self.broj_indeksa = broj_indeksa
        self.godina_upisa = godina_upisa
        self.smer = smer
        self.grupa = grupa

def populate():
    studenti = [
        StudentInfo("Anamarija", "Krstic", 117, 2018, "RN", 302),
        StudentInfo("Tamara", "Sarac", 0, 2016, "RN", 301),
        StudentInfo("Mariano", "Suban", 37, 2014, "RN", 0)
    ]

    semestar, created = Semestar.objects.get_or_create(vrsta="neparni", skolska_godina_pocetak=2018, skolska_godina_kraj=2019)

    for student in studenti:
        print("Dodavanje studenta %s %s" % (student.ime, student.prezime))
        ime_naloga = (student.ime[0] + student.prezime + str(student.godina_upisa)[2:4]).replace(" ", "").lower()
        nalog, created = Nalog.objects.get_or_create(username=ime_naloga, uloga="student")
        grupa, created = Grupa.objects.get_or_create(oznaka_grupe=student.grupa, semestar=semestar)
        new_student, created = Student.objects.get_or_create(
            ime=student.ime,
            prezime=student.prezime,
            broj_indeksa=student.broj_indeksa,
            godina_upisa=student.godina_upisa,
            smer=student.smer,
            nalog=nalog
        )
        new_student.grupa.add(grupa)

if __name__ == '__main__':
    print("Dodavanje studenata....")
    populate()
