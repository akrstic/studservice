Student Service
==================

This is a project made for our Script Languages class. It uses Django on the server side and PostgreSQL as the database.

Setting up the project
------------------

Make sure you have Python, Django and PostgreSQL installed on your PC.

For Ubuntu Python comes preinstalled. Follow this tutorial to set up Django: https://www.howtoforge.com/tutorial/how-to-install-django-on-ubuntu/. To install PostgreSQL run

```
sudo apt-get install postgresql postgresql-contrib
sudo pip install psycopg2-binary
python -c "import psycopg2"
```

For macOS `brew install python` and then follow this tutorial to set up Django: https://www.codingforentrepreneurs.com/blog/install-django-on-mac-or-linux/. Set up PostgreSQL using https://postgresapp.com/.

For Manjaro, Arch and other Arch-based operating systems:

```
  sudo pacman -Sy python python-django postgresql
  sudo pip install psycopg2-binary
  python -c "import psycopg2"
```

To access the Django shell run `python manage.py shell`.

Don't forget to install Django Bootstrap by running

```
sudo pip install django-bootstrap3
```

Managing the database
------------------

A good tutorial to setting up PostgreSQL is this: https://tutorial-extensions.djangogirls.org/en/optional_postgresql_installation/.

---

*Only for Arch-based OS*

Run the command `sudo -u postgres -i`. When there start up the database cluster by running `initdb -D '/var/lib/postgres/data'`. In another tab start and enable the postgresql.service by running the following commands:

```
systemctl start postgresql.service
systemctl enable postgresql.service
```

*End of Arch specific stuff*

---

Now we need to create a postgres user that has the same name as your login user. Run the commands

```
createuser -s -U postgres --interactive
Enter name of role to add: myUsualLoginName
```
and set up the user with the same name as your login name.

Next we need to create our database by running `createdb studservice`.

To access the database shell run `psql -d studservice`. Once in the shell create the user `studservice` and grant them all privileges by running:

 ```
 CREATE USER studservice WITH PASSWORD 'studservice';
 GRANT ALL PRIVILEGES ON DATABASE studservice TO studservice;
 ```

To leave the shell run `\q`.

If everything was set up correctly running the command `python manage.py migrate` should work properly.

When changes to the model are made to update the database you'll need to run the following commands:

```
python manage.py makemigrations studserviceapp
python manage.py migrate
```

Importing initial data into the database
------------------

In the python shell (`python manage.py shell`) run the following commands to populate the database with data from 'rasporedCSV.csv':

```
from studserviceapp.utils import import_timetable_from_csv
import_timetable_from_csv("rasporedCSV.csv")

```

There is also the script called 'populate_students.py' where you can add a new StudentInfo class into the script and populate student information from there by running `python populate_students.py`.

Running the server
------------------

To start the server run the following command in the base folder:

```
  python manage.py runserver
```
The server starts up on `localhost:8000` by default. To change the default port run:

```
  python manage.py runserver ${PORT}
```

Viewing the project
------------------

The API starts up on http://localhost:8000/studserviceapp/login

You can access individual timetables by visiting http://localhost:8000/studserviceapp/timetable/akrstic18 as an example
